
using System;
using System.Collections.Generic;
using System.Text;

namespace ServiceWarehouse
{
    /// <summary>
    /// 【类】PurchaseOrderMain,【描述】商家订单
    /// </summary> 
    public class PurchaseOrderMain 
    {
        
        /// <summary>
        /// 【属性】ID,【描述】ID
        /// </summary>     
        public int ID {get;set;}

        /// <summary>
        /// 【属性】AreaCode,【描述】地区
        /// </summary>     
        public int AreaCode {get;set;}

        /// <summary>
        /// 【属性】ShopID,【描述】商家账号
        /// </summary>     
        public long ShopID {get;set;}

        /// <summary>
        /// 【属性】ShopName,【描述】商家名称
        /// </summary>     
        public string ShopName {get;set;}

        /// <summary>
        /// 【属性】ToPurchaseTime,【描述】订单日期
        /// </summary>     
        public string ToPurchaseTime {get;set;}

        /// <summary>
        /// 【属性】ExceptAmount,【描述】预计金额（昨日价格*订货数）
        /// </summary>     
        public double ExceptAmount {get;set;}

        /// <summary>
        /// 【属性】FrozenAmount,【描述】冻结金额
        /// </summary>     
        public double FrozenAmount {get;set;}

        /// <summary>
        /// 【属性】ActualAmount,【描述】实际金额
        /// </summary>     
        public double ActualAmount {get;set;}

        /// <summary>
        /// 【属性】ArrivalAmount,【描述】到货金额
        /// </summary>     
        public double ArrivalAmount {get;set;}

        /// <summary>
        /// 【属性】Freight,【描述】运费
        /// </summary>     
        public double Freight {get;set;}

        /// <summary>
        /// 【属性】OrderStatus,【描述】订单状态
        /// </summary>     
        public int OrderStatus {get;set;}

        /// <summary>
        /// 【属性】ArrivedStatus,【描述】到货状态
        /// </summary>     
        public int ArrivedStatus {get;set;}

        /// <summary>
        /// 【属性】OrderTime,【描述】下单时间
        /// </summary>     
        public string OrderTime {get;set;}

        /// <summary>
        /// 【属性】PurchaseTime,【描述】采购时间
        /// </summary>     
        public string PurchaseTime {get;set;}

        /// <summary>
        /// 【属性】DeliverTime,【描述】发货时间
        /// </summary>     
        public string DeliverTime {get;set;}

        /// <summary>
        /// 【属性】PickUpType,【描述】提货方式
        /// </summary>     
        public bool PickUpType {get;set;}

        /// <summary>
        /// 【属性】DriverName,【描述】司机姓名
        /// </summary>     
        public string DriverName {get;set;}

        /// <summary>
        /// 【属性】DriverPhone,【描述】司机电话
        /// </summary>     
        public string DriverPhone {get;set;}

        /// <summary>
        /// 【属性】DriverPhone,【描述】商家手机
        /// </summary>     
        public string ShopPhone {get;set;}

        /// <summary>
        /// 【属性】DriverPhone,【描述】商家地址
        /// </summary>     
        public string ShopAddress {get;set;}

        /// <summary>
        /// 【属性】PurchaseOrderSub,【描述】订单明细
        /// </summary>     
        public List<PurchaseOrderSub> PurchaseOrderSub {get;set;}

    }
}