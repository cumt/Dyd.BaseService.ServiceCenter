
using System;
using System.Collections.Generic;
using System.Text;

namespace AutoServiceDemoJustCall
{
    /// <summary>
    /// 【服务】AutoServiceDemoJustCall,【描述】测试调用性能
    /// </summary>
    public class IClearErrorClient:XXF.BaseService.ServiceCenter.Client.RemoteClient.ThriftRemoteClient
    {
        public IClearErrorClient()
        {
        }

        
        /// <summary>
        /// 【方法】AutoServiceDemoJustCall,【描述】测试调用性能
        /// </summary>
           
        public int JustCall(int i)
        {
            return Call<int>("JustCall",new object[]{i}); 
        }

        /// <summary>
        /// 【方法】AutoServiceDemo,【描述】AutoServiceDemo
        /// </summary>
           
        public List<OrderMain> ClearOne()
        {
            return Call<List<OrderMain>>("ClearOne",new object[]{}); 
        }

       
    }
}
