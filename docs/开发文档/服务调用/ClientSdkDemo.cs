﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Demo.Client
{
    public class ClientSdkDemo
    {
        public void Run()
        {
            /*
             * 0.到配置中心配置"ServiceCenterConnectString" 服务中心连接
             * 1.从服务中心获取要调用的服务的"命名空间"
             * 2.确认该服务已经公开"协议"。
             * 3.将服务生成到本地sdk
             */
            //一句代码,生成sdk到本地.
            XXF.BaseService.ServiceCenter.Client.ClientSDKHelper.ToLocalFile("AutoServiceDemo",
                @"E:\working\BasicService\Dyd.BaseService.ServiceCenter\Dyd.BaseService.ServiceCenter.Demo.Client\ServiceSDKs\Dyd.BaseService.ServiceCenter.Demo.Service\");
        }
    }
}
