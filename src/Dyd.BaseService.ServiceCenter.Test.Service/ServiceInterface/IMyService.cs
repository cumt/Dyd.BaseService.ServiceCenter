﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    [XXF.BaseService.ServiceCenter.Service.OpenDoc.ServiceDoc("测试服务", "我的测试服务")]
    public interface IMyService
    {
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.MethodDoc("测试方法", "我的测试方法", "", "字符串", "车", "2012-8-24", "无")]
        string Test();
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.MethodDoc("测试实体", "我的测试的实体方法", "entity:测试实体;", "字符串", "车", "2012-8-24", "无")]

        MyEntity1 ToDo(MyEntity1 entity);
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.MethodDoc("测试实体", "我的测试的实体方法", "entity:测试实体;", "字符串", "车", "2012-8-24", "无")]

        MyEntity2 ToDo2(MyEntity2 entity);
        [XXF.BaseService.ServiceCenter.Service.OpenDoc.MethodDoc("测试实体", "我的测试的实体方法", "entity:测试实体;", "字符串", "车", "2012-8-24", "无")]
        bool ToDo3(int entity);
    }
}
