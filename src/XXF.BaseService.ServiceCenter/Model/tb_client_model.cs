using System;
using System.Xml.Serialization;
using System.Collections.Generic;

namespace XXF.BaseService.ServiceCenter.Model
{
    /// <summary>
    /// tb_client Data Structure.
    /// </summary>
    [Serializable]
    public partial class tb_client_model
    {
	/*代码自动生成工具自动生成,不要在这里写自己的代码，否则会被自动覆盖哦 - 车毅*/
        
        /// <summary>
        /// 
        /// </summary>
        public int id { get; set; }
        
        /// <summary>
        /// 
        /// </summary>
        public int serverid { get; set; }
        
        /// <summary>
        /// 会话id
        /// </summary>
        public long sessionid { get; set; }
        
        /// <summary>
        /// 项目名
        /// </summary>
        public string projectname { get; set; }
        
        /// <summary>
        /// ip地址
        /// </summary>
        public string ip { get; set; }
        
        /// <summary>
        /// 客户端心跳时间
        /// </summary>
        public DateTime clientheartbeattime { get; set; }
        
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime createtime { get; set; }
        
    }
}