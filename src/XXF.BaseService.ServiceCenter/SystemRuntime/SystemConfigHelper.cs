﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BaseService.ServiceCenter.Dal;
using XXF.BaseService.ServiceCenter.Model;
using XXF.Db;
using XXF.ProjectTool;

namespace XXF.BaseService.ServiceCenter.SystemRuntime
{
    /// <summary>
    /// 系统配置帮助类
    /// </summary>
    public class SystemConfigHelper
    {
        public static string ServiceCenterConnectString { get { return System.Configuration.ConfigurationManager.AppSettings["ServiceCenterConnectString"]; } }
        public static string RedisServer { get; set; }
        public static string ZooKeeperServer { get; set; }
        public static string ReportConnectString { get; set; }
        public static int NodeMinPort { get; set; }
        public static int NodeMaxPort { get; set; }
        public static string WebSite { get; set; }

        public static void Load()
        {
            SqlHelper.ExcuteSql(ServiceCenterConnectString, (conn) =>
            {
                List<tb_system_config_model> configs = new tb_system_config_dal().GetAllList(conn);
                foreach (var c in configs)
                {
                    if (c.key.ToLower() == EnumSystemConfigKey.RedisServer.ToString().ToLower())
                    {
                        RedisServer = c.value;
                    }
                    else if (c.key.ToLower() == EnumSystemConfigKey.ZooKeeperServer.ToString().ToLower())
                    {
                        ZooKeeperServer = c.value;
                    }
                    else if (c.key.ToLower() == EnumSystemConfigKey.ReportConnectString.ToString().ToLower())
                    {
                        ReportConnectString = c.value;
                    }
                    else if (c.key.ToLower() == EnumSystemConfigKey.NodeMinPort.ToString().ToLower())
                    {
                        NodeMinPort = Convert.ToInt32(c.value);
                    }
                    else if (c.key.ToLower() == EnumSystemConfigKey.NodeMaxPort.ToString().ToLower())
                    {
                        NodeMaxPort = Convert.ToInt32(c.value);
                    }
                    else if (c.key.ToLower() == EnumSystemConfigKey.WebSite.ToString().ToLower())
                    {
                        WebSite = Convert.ToString(c.value);
                    }
                }
            });
        }
    }
}
