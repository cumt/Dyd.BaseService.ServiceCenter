
using System;
using System.Collections.Generic;
using System.Text;

namespace Dyd.BaseService.ServiceCenter.Test.Service
{
    /// <summary>
    /// 【服务】测试服务,【描述】我的测试服务
    /// </summary>
    public class IMyServiceClient:XXF.BaseService.ServiceCenter.Client.RemoteClient.ThriftRemoteClient
    {
        public IMyServiceClient()
        {
        }

        
        /// <summary>
        /// 【方法】测试方法,【描述】我的测试方法
        /// </summary>
           
        public string Test()
        {
            return Call<string>("Test",new object[]{}); 
        }

        /// <summary>
        /// 【方法】测试实体,【描述】我的测试的实体方法
        /// </summary>
       /// <param name="entity">测试实体</param>    
        public MyEntity1 ToDo(MyEntity1 entity)
        {
            return Call<MyEntity1>("ToDo",new object[]{entity}); 
        }

        /// <summary>
        /// 【方法】测试实体,【描述】我的测试的实体方法
        /// </summary>
       /// <param name="entity">测试实体</param>    
        public MyEntity2 ToDo2(MyEntity2 entity)
        {
            return Call<MyEntity2>("ToDo2",new object[]{entity}); 
        }

        /// <summary>
        /// 【方法】测试实体,【描述】我的测试的实体方法
        /// </summary>
       /// <param name="entity">测试实体</param>    
        public void ToDo3(int entity)
        {
            Call<bool>("ToDo3",new object[]{entity}); 
        }

       
    }
}
